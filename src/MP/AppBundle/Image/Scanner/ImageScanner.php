<?php

namespace MP\AppBundle\Image\Scanner;

use MP\AppBundle\Entity\Image;

class ImageScanner {
    
    public function ScanImage($step = 100) {
        $imgpath = __DIR__ . '/../../Resources/public/images/source.jpg';
        $imgsize = getimagesize($imgpath);
        $im = imagecreatefromjpeg($imgpath);
        $width = $imgsize[0];
        $height = $imgsize[1];
        
        $imgs = array();
        for ($y = 0; $y < $height; $y+= $step) {
            for ($x = 0; $x < $width; $x+= $step) {
                $isnew = TRUE;
                foreach ($imgs as $img) {
                    if ($this->CheckIn($img, $x, $y)) {
                        $isnew = FALSE;
                    }
                }
                
                $color = imagecolorat($im, $x, $y);
                
                if (!$this->CheckColor($color) && $isnew) {
                    $image = new Image();
                    $imgs[] = $image;
                    $imgdatas = $this->ScanImageDatas($im, $x, $y);
                    $position = array($imgdatas[0], $imgdatas[1]);
                    $image->setPosition($position);
                    $size = array($imgdatas[2], $imgdatas[3]);
                    $image->setSize($size);
                    $isnew = FALSE;
                }
            }
        }
        imagedestroy($im);
        return $imgs;
    }
    
    private function ScanImageDatas($im, $x, $y) {
        $startpos = array($x, $y);
        $imgdatas = array(0, 0, 0, 0);
        $color = imagecolorat($im, $x, $y);
        while (!$this->CheckColor($color)) {
            $x++;
            $imgdatas[2]++;
            $color = imagecolorat($im, $x, $y);
        }
        $x = $startpos[0];
        $color = imagecolorat($im, $x, $y);
        while (!$this->CheckColor($color)) {
            $x--;
            $imgdatas[2]++;
            $color = imagecolorat($im, $x, $y);
        }
        $x++;
        $color = imagecolorat($im, $x, $y);
        while (!$this->CheckColor($color)) {
            $y++;
            $imgdatas[3]++;
            $color = imagecolorat($im, $x, $y);
        }
        $y = $startpos[1];
        $color = imagecolorat($im, $x, $y);
        while (!$this->CheckColor($color)) {
            $y--;
            $imgdatas[3]++;
            $color = imagecolorat($im, $x, $y);
        }
        $y++;
        $imgdatas[0] = $x;
        $imgdatas[1] = $y;
        return $imgdatas;
    }
    
    private function CheckIn($img, $x, $y) {
        return (($img->getPosition()[0] <= $x && 
                $x <= ($img->getPosition()[0] + $img->getSize()[0])) &&
                ($img->getPosition()[1] <= $y && 
                $y <= ($img->getPosition()[1] + $img->getSize()[1])));
    }
    
    private function CheckColor($color) {
        $rgb["r"] = ($color >> 16) & 0xFF;
        $rgb["g"] = ($color >> 8) & 0xFF;
        $rgb["b"] = $color & 0xFF;
        return (248 <= $rgb["r"] && $rgb["r"] <= 255) &&
            (248 <= $rgb["g"] && $rgb["g"] <= 255) &&
            (248 <= $rgb["b"] && $rgb["b"] <= 255);
    }

}

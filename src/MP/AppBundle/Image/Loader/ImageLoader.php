<?php

namespace MP\AppBundle\Image\Loader;

use MP\AppBundle\Image\Scanner\ImageScanner;

class ImageLoader {
    
    private $imgscanner;
    
    public function __construct(ImageScanner $imgscanner) {
        $this->imgscanner = $imgscanner;
    }
    
    public function loadImages() {
        if (!file_exists(__DIR__ . '/../../Resources/public/images/splits')) {
            mkdir(__DIR__ . '/../../Resources/public/images/splits', 0777, true);
        }
        if (!file_exists(__DIR__ . '/../../Resources/public/images/colored')) {
            mkdir(__DIR__ . '/../../Resources/public/images/colored', 0777, true);
        }
        if ($this->is_dir_empty(__DIR__ . '/../../Resources/public/images/splits') ||
                $this->is_dir_empty(__DIR__ . '/../../Resources/public/images/colored')) {
            $imgdatas = $this->imgscanner->ScanImage();
            usort($imgdatas, array($this, "sort_objects_by_size"));
            $imgpath = __DIR__ . '/../../Resources/public/images/source.jpg';
            $imgsize = getimagesize($imgpath);
            $im = imagecreatefromjpeg($imgpath);
            $i = 1;
            $color = imagecolorallocatealpha($im, 255,0,0,85);
            foreach ($imgdatas as $imgdata) {
                $datas = array(
                    'x' => $imgdata->getPosition()[0],
                    'y' => $imgdata->getPosition()[1],
                    'width' => $imgdata->getSize()[0], 
                    'height' => $imgdata->getSize()[1]);
                $split = imagecrop($im, $datas);
                imagefilledrectangle($im, 
                        $datas['x'],
                        $datas['y'],
                        $datas['x'] + $datas['width'],
                        $datas['y'] + $datas['height'],
                        $color );

                imagejpeg($split, __DIR__ . "/../../Resources/public/images/splits/split$i.jpg", 100);
                imagedestroy($split);
                if ($i == 9) {
                 $i = 90;
                }
                $i++;
            }
            imagejpeg($im, __DIR__ . "/../../Resources/public/images/colored/colored.jpg", 100);
            imagedestroy($im);
        }
    }
    
    private function is_dir_empty($dir) {
        if (!is_readable($dir))
            return NULL;
        $handle = opendir($dir);
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                return FALSE;
            }
        }
        return TRUE;
    }
    
    private function sort_objects_by_size($a, $b) {
	if($a->getSize() == $b->getSize()){ 
            return 0 ; 
        }
	return ($a->getSize() > $b->getSize()) ? -1 : 1;
    }

}

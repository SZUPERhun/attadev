<?php

namespace MP\AppBundle\Entity;

class Image
{
    private $position;

    private $size;

    public function getId()
    {
        return $this->id;
    }
    
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }
    
    public function getSize()
    {
        return $this->size;
    }
}


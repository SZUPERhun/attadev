<?php

namespace MP\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MP\AppBundle\Scanner\ImageScanner;
use FilesystemIterator;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $imgloader = $this->get('mp_img.loader.imgloader');
        $imgs = $imgloader->loadImages();

        return $this->render('AppBundle:Default:index.html.twig', array());
    }
    
    
}

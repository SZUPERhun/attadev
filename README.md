Attadev
=======

Attadev által adott tesztfeladat. Feladat lényege az volt, hogy egy képből dinamikusan elő kellett állítani több kisebbet.
A kisképeket szélesség alapján csökkenő sorrendben megjeleníteni és drag and dropolhatóvá tenni őket, 
emellett a nagy képet úgy kellett megjeleníteni, hogy a kisképekre egy vörös átlátszó réteget kellet rakni.
A webalkalmazás Symfony keretrendszerben készült.

Az alábbi parancsokkal könnyen telepíthető, frissíthető és futtatható az alkalmazás.
Az alábbi címen érhető el az oldal:
> **http://127.0.0.1:8000**

# Parancsok

Telepítés
---------------------------
> **sh symfony.sh install**

Frissítés
---------------------------
> **sh symfony.sh update**

Futtatás
---------------------------
> **sh symfony.sh run**

A Symfony project created on September 25, 2015, 5:50 pm.
